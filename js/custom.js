/* 
    
    1. Customer Slider JS 
    2. feed Slider JS 
    3. screenshotSlider  JS
    4. Moretext  JS
    5. Search  JS
    6. Tooltip JS

    */

    $(document).ready(function () {

        /* 1. Customer Slider JS */

        var Swipes = new Swiper('.customerSlider', {
            loop: false,
            slidesPerView: 2,
            spaceBetween: 13,
            speed: 1500,


        // Navigation arrows
        navigation: {
            nextEl: ".next-slide",
            prevEl: ".prev-slide"
        },

        breakpoints:
        {

            1200: {
                slidesPerView: 2,

            },
            320: {
                slidesPerView: 1,

            }
        }
    });

        /* 2. feed Slider JS */

        var Swipes = new Swiper('.feedSlider', {
            loop: false,
            slidesPerView: 3,
            spaceBetween: 13,
            speed: 1500,


        // Navigation arrows
        navigation: {
            nextEl: ".next-slide",
            prevEl: ".prev-slide"
        },
        // pagination arrows
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },

        breakpoints:
        {

            992: {
                slidesPerView: 3,

            },

            768: {
                slidesPerView: 2,

            },
            320: {
                slidesPerView: 1,

            }
        }
    });


        /* 3. screenshotSlider  JS */

        var Swipes = new Swiper('.screenshotSlider', {
            loop: false,
            slidesPerView: 5,
            spaceBetween: 13,
            speed: 1500,
            simulateTouch: false,
            autoHeight: false,
            breakpoints:
            {

                1200: {
                    slidesPerView: 5,
                    simulateTouch: false,

                },

                992: {
                    slidesPerView: 4,
                    simulateTouch: true,
                    autoHeight: false,

                },
                641: {
                    slidesPerView: 3,

                },
                480: {
                    slidesPerView: 2,

                },
                320: {
                    slidesPerView: 1,
                    simulateTouch: true,
                    autoHeight: true,

                }
            }
        });

        /* 4. Moretext  JS */

        $('.moretext').click(function () {

            $(this).parent().parent().find('.readmorecontentnew').slideToggle('slow');
            if ($(this).text() == 'Learn Less') {
                $(this).text('Learn More').removeClass('readless').addClass('readmorenew');
            }
            else {
                $(this).text('Learn Less').addClass('readless').removeClass('readmorenew');
            }
        });

        $('.moretextnew').click(function () {

            $(this).parent().parent().find('.readmorecontentnew').slideToggle('slow');
            if ($(this).text() == 'Learn Less') {
                $(this).text('Learn More').removeClass('readless').addClass('readmorenew');
            }
            else {
                $(this).text('Learn Less').addClass('readless').removeClass('readmorenew');
            }
        });

        /* 5. Search  JS */

        $(".icon-search").click(function () {
            $(".togglesearch").toggle();
            $("input[type='text']").focus();
        });


        /* 6. Tooltip JS */

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });


    });



// Toggle down close icon on show hide of collapse element
$("#accordion").on("hide.bs.collapse show.bs.collapse", function (e) {
    $(e.target)
    .prev()
    .find("span")
    .toggleClass("fa-angle-up fa-angle-down");

});

// Toggle active card on show hide of collapse element
$(".collapse").on('show.bs.collapse', function () {
    $(this).prev(".card-header").parent().addClass("active");
}).on('hide.bs.collapse', function () {
    $(this).prev(".card-header").parent().removeClass("active");
});


$(function () {
    $('[data-toggle="popover"]').popover()
})


$(".icon-wrap .fa-thumbs-down").click(function () {
    $(this).toggleClass("fas");
    $(this).toggleClass("selected");
});
$(".heart .fa-heart").click(function () {
    $(this).toggleClass("fas");
    $(this).toggleClass("selected");
});


// ==data-value==
$(function () {

    $(".progress.circle").each(function () {

        var value = $(this).attr('data-value');
        var left = $(this).find('.progress-left .progress-bar');
        var right = $(this).find('.progress-right .progress-bar');

        if (value > 0) {
            if (value <= 50) {
                right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
            } else {
                right.css('transform', 'rotate(180deg)')
                left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
            }
        }

    })

    function percentageToDegrees(percentage) {

        return percentage / 100 * 360

    }

});

// ----Drag input file

var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

// highlight drag area
$fileInput.on('dragenter focus click', function () {
    $droparea.addClass('is-active');
});

// back to normal state
$fileInput.on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
});

// change inner text
$fileInput.on('change', function () {
    var filesCount = $(this)[0].files.length;
    var $textContainer = $(this).prev();

    if (filesCount === 1) {
        // if single file is selected, show file name
        var fileName = $(this).val().split('\\').pop();
        $textContainer.text(fileName);
    } else {
        // otherwise show number of files
        $textContainer.text(filesCount + ' files selected');
    }
});


// $(function () {
//     $('#contacts .contact').click(function () {
//         $(this).siblings().removeClass('active');
//         $(this).addClass('active');


//         $(".messages .chat").addClass('active').siblings().removeClass('active');
//     });
// });


$("#contacts .contact").click(function () {
    $(this).hasClass("active");
    $(".mobile-wrap").toggleClass('active');
});
$(".mobile-wrap .back").click(function () {
    $(".mobile-wrap").removeClass('active');
});



$(function () {
    var $radioButtons = $('.main-hcekbox input[type="radio"]');
    $radioButtons.click(function () {
        $radioButtons.each(function () {
            $(this).parent().parent().toggleClass('active', this.checked);
        });
    });
});